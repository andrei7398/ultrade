package com.ultrade.repository;

import com.ultrade.domain.Advertisement;

import com.ultrade.domain.Person;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Advertisement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Long> {

    List<Advertisement> findAllBySeller(Person person);

    void deleteAllByProductId(long id);
}
