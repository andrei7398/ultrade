package com.ultrade.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "gender")
    private String gender;

    @Column(name = "number_of_photos")
    private Integer numberOfPhotos;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @OneToOne
    @JoinColumn(unique = true)
    private ProductType productType;

    @OneToOne
    @JoinColumn(unique = true)
    private ProductSize productSize;

    @JsonProperty("photos")
    @OneToMany(mappedBy = "product", fetch=FetchType.EAGER)
    private Set<Photo> photos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public Product gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getNumberOfPhotos() {
        return numberOfPhotos;
    }

    public Product numberOfPhotos(Integer numberOfPhotos) {
        this.numberOfPhotos = numberOfPhotos;
        return this;
    }

    public void setNumberOfPhotos(Integer numberOfPhotos) {
        this.numberOfPhotos = numberOfPhotos;
    }

    public String getName() {
        return name;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getProductType() {
        return productType;
    }

    public Product productType(ProductType productType) {
        this.productType = productType;
        return this;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public ProductSize getProductSize() {
        return productSize;
    }

    public Product productSize(ProductSize productSize) {
        this.productSize = productSize;
        return this;
    }

    public void setProductSize(ProductSize productSize) {
        this.productSize = productSize;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public Product photos(Set<Photo> photos) {
        this.photos = photos;
        return this;
    }

    public Product addPhoto(Photo photo) {
        this.photos.add(photo);
        photo.setProduct(this);
        return this;
    }

    public Product removePhoto(Photo photo) {
        this.photos.remove(photo);
        photo.setProduct(null);
        return this;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return id != null && id.equals(((Product) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", gender='" + getGender() + "'" +
            ", numberOfPhotos=" + getNumberOfPhotos() +
            ", name='" + getName() + "'" +
            "}";
    }
}
