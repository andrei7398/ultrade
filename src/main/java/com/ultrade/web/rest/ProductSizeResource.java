package com.ultrade.web.rest;

import com.ultrade.domain.ProductSize;
import com.ultrade.service.ProductSizeService;

import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ultrade.domain.ProductSize}.
 */
@RestController
@RequestMapping("/api")
public class ProductSizeResource {

    private final Logger log = LoggerFactory.getLogger(ProductSizeResource.class);

    private final ProductSizeService productSizeService;

    public ProductSizeResource(ProductSizeService productSizeService) {
        this.productSizeService = productSizeService;
    }

    /**
     * {@code GET  /product-sizes} : get all the productSizes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productSizes in body.
     */
    @GetMapping("/product-sizes")
    public List<ProductSize> getAllProductSizes() {
        log.debug("REST request to get all ProductSizes");
        return productSizeService.findAll();
    }

    /**
     * {@code GET  /product-sizes/:id} : get the "id" productSize.
     *
     * @param id the id of the productSize to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productSize, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-sizes/{id}")
    public ResponseEntity<ProductSize> getProductSize(@PathVariable Long id) {
        log.debug("REST request to get ProductSize : {}", id);
        Optional<ProductSize> productSize = productSizeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productSize);
    }
}
