package com.ultrade.web.rest;

import com.ultrade.domain.Hashtag;
import com.ultrade.service.HashtagService;

import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ultrade.domain.Hashtag}.
 */
@RestController
@RequestMapping("/api")
public class HashtagResource {

    private final Logger log = LoggerFactory.getLogger(HashtagResource.class);

    private final HashtagService hashtagService;

    public HashtagResource(HashtagService hashtagService) {
        this.hashtagService = hashtagService;
    }

    /**
     * {@code GET  /hashtags} : get all the hashtags.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hashtags in body.
     */
    @GetMapping("/hashtags")
    public List<Hashtag> getAllHashtags() {
        log.debug("REST request to get all Hashtags");
        return hashtagService.findAll();
    }

    /**
     * {@code GET  /hashtags/:id} : get the "id" hashtag.
     *
     * @param id the id of the hashtag to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hashtag, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hashtags/{id}")
    public ResponseEntity<Hashtag> getHashtag(@PathVariable Long id) {
        log.debug("REST request to get Hashtag : {}", id);
        Optional<Hashtag> hashtag = hashtagService.findOne(id);
        return ResponseUtil.wrapOrNotFound(hashtag);
    }
}
