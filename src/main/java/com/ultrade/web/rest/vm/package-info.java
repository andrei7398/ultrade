/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ultrade.web.rest.vm;
