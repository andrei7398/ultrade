package com.ultrade.service;

import com.ultrade.domain.Photo;
import com.ultrade.repository.PhotoRepository;
import com.ultrade.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Photo}.
 */
@Service
@Transactional
public class PhotoService {

    private final Logger log = LoggerFactory.getLogger(PhotoService.class);

    private final PhotoRepository photoRepository;

    private static final String LOCAL_STORAGE_PATH = "C:\\Users\\ASUS ROG\\Downloads\\";


    public PhotoService(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    /**
     * Save a photo.
     *
     * @param photo the entity to save.
     * @return the persisted entity.
     */
    public Photo save(Photo photo) {
        log.debug("Request to save Photo : {}", photo);
        Photo result = photoRepository.save(photo);
        savePhotoLocally(result, photo.getPhoto());
        return result;
    }

    /**
     * Get all the photos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Photo> findAll() {
        log.debug("Request to get all Photos");
        return photoRepository.findAll();
    }

    /**
     * Get one photo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Photo> findOne(Long id) {
        log.debug("Request to get Photo : {}", id);
        return photoRepository.findById(id);
    }

    /**
     * Delete the photo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Photo : {}", id);
        photoRepository.deleteById(id);
    }

    public void savePhotoLocally(Photo photo, byte[] photoByteArray) {

        String fileNameWithPath = LOCAL_STORAGE_PATH + "photo_" + photo.getId() + photo.getExtension();

        File file = new File(fileNameWithPath);

        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(photoByteArray);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        if (multipartFile.getSize() > 1024) {
//            throw new BadRequestAlertException("Exceeds maximum size", ENTITY_NAME, "idnull");
//        }
//        if (multipartFile.getOriginalFilename() == null || multipartFile.getOriginalFilename().length() == 0) {
//            throw new BadRequestAlertException("File name problem", ENTITY_NAME, "idnull");
//        }
//
//        String originalFileName = multipartFile.getOriginalFilename();
//        int dot = originalFileName.lastIndexOf('.');
//        String extension = (dot == -1) ? "" : originalFileName.substring(dot + 1);
//
//        List<String> validExtentions = Arrays.asList(".png", ".jpg", ".jpeg");
//        if (!validExtentions.contains(extension.toLowerCase())) {
//            throw new BadRequestAlertException("Invalid photos extensions", ENTITY_NAME, "idnull");
//        }
//
//        String fileNameWithPath = LOCAL_STORAGE_PATH + originalFileName;
//
//        File file = new File(fileNameWithPath);
//
//        byte[] buf = new byte[1024];
//
//        try (InputStream inputStream = multipartFile.getInputStream();
//             FileOutputStream fileOutputStream = new FileOutputStream(file)) {
//
//            int numRead = 0;
//            while ((numRead = inputStream.read(buf)) >= 0) {
//                fileOutputStream.write(buf, 0, numRead);
//            }
//        }
    }
}
