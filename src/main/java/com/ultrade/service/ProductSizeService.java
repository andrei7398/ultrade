package com.ultrade.service;

import com.ultrade.domain.ProductSize;
import com.ultrade.repository.ProductSizeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ProductSize}.
 */
@Service
@Transactional
public class ProductSizeService {

    private final Logger log = LoggerFactory.getLogger(ProductSizeService.class);

    private final ProductSizeRepository productSizeRepository;

    public ProductSizeService(ProductSizeRepository productSizeRepository) {
        this.productSizeRepository = productSizeRepository;
    }

    /**
     * Save a productSize.
     *
     * @param productSize the entity to save.
     * @return the persisted entity.
     */
    public ProductSize save(ProductSize productSize) {
        log.debug("Request to save ProductSize : {}", productSize);
        ProductSize result = productSizeRepository.save(productSize);
        return result;
    }

    /**
     * Get all the productSizes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ProductSize> findAll() {
        log.debug("Request to get all ProductSizes");
        return productSizeRepository.findAll();
    }

    /**
     * Get one productSize by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProductSize> findOne(Long id) {
        log.debug("Request to get ProductSize : {}", id);
        return productSizeRepository.findById(id);
    }

    /**
     * Delete the productSize by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductSize : {}", id);
        productSizeRepository.deleteById(id);
    }
}
