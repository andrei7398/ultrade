package com.ultrade.service;

import com.ultrade.domain.Advertisement;
import com.ultrade.repository.AdvertisementRepository;
import com.ultrade.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Advertisement}.
 */
@Service
@Transactional
public class AdvertisementService {

    private final Logger log = LoggerFactory.getLogger(AdvertisementService.class);

    private final AdvertisementRepository advertisementRepository;

    private final ProductRepository productRepository;

    public AdvertisementService(AdvertisementRepository advertisementRepository, ProductRepository productRepository) {
        this.advertisementRepository = advertisementRepository;
        this.productRepository = productRepository;
    }

    /**
     * Save a advertisement.
     *
     * @param advertisement the entity to save.
     * @return the persisted entity.
     */
    public Advertisement save(Advertisement advertisement) {
        log.debug("Request to save Advertisement : {}", advertisement);
        long productId = advertisement.getProduct().getId();
        productRepository.findById(productId).ifPresent(advertisement::product);
        Advertisement result = advertisementRepository.save(advertisement);
        return result;
    }

    /**
     * Get all the advertisements.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Advertisement> findAll() {
        log.debug("Request to get all Advertisements");
        return advertisementRepository.findAll();
    }

    /**
     * Get one advertisement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Advertisement> findOne(Long id) {
        log.debug("Request to get Advertisement : {}", id);
        return advertisementRepository.findById(id);
    }

    /**
     * Delete the advertisement by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Advertisement : {}", id);
        advertisementRepository.deleteById(id);
    }
}
