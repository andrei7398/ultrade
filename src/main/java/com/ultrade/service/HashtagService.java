package com.ultrade.service;

import com.ultrade.domain.Hashtag;
import com.ultrade.repository.HashtagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Hashtag}.
 */
@Service
@Transactional
public class HashtagService {

    private final Logger log = LoggerFactory.getLogger(HashtagService.class);

    private final HashtagRepository hashtagRepository;

    public HashtagService(HashtagRepository hashtagRepository) {
        this.hashtagRepository = hashtagRepository;
    }

    /**
     * Save a hashtag.
     *
     * @param hashtag the entity to save.
     * @return the persisted entity.
     */
    public Hashtag save(Hashtag hashtag) {
        log.debug("Request to save Hashtag : {}", hashtag);
        Hashtag result = hashtagRepository.save(hashtag);
        return result;
    }

    /**
     * Get all the hashtags.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Hashtag> findAll() {
        log.debug("Request to get all Hashtags");
        return hashtagRepository.findAll();
    }

    /**
     * Get one hashtag by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Hashtag> findOne(Long id) {
        log.debug("Request to get Hashtag : {}", id);
        return hashtagRepository.findById(id);
    }

    /**
     * Delete the hashtag by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Hashtag : {}", id);
        hashtagRepository.deleteById(id);
    }
}
