package com.ultrade.service;

import com.ultrade.domain.Product;
import com.ultrade.repository.AdvertisementRepository;
import com.ultrade.repository.PhotoRepository;
import com.ultrade.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Product}.
 */
@Service
@Transactional
public class ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    private final ProductRepository productRepository;
    private final AdvertisementRepository advertisementRepository;
    private final PhotoRepository photoRepository;

    public ProductService(ProductRepository productRepository, AdvertisementRepository advertisementRepository, PhotoRepository photoRepository) {
        this.productRepository = productRepository;
        this.advertisementRepository = advertisementRepository;
        this.photoRepository = photoRepository;
    }

    /**
     * Save a product.
     *
     * @param product the entity to save.
     * @return the persisted entity.
     */
    public Product save(Product product) {
        log.debug("Request to save Product : {}", product);
        Product result = productRepository.save(product);
        return result;
    }

    /**
     * Get all the products.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        log.debug("Request to get all Products");
        return productRepository.findAll();
    }

    /**
     * Get one product by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Product> findOne(Long id) {
        log.debug("Request to get Product : {}", id);
        return productRepository.findById(id);
    }

    /**
     * Delete the product by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Product : {}", id);

        advertisementRepository.deleteAllByProductId(id);
        photoRepository.deleteAllByProductId(id);

        productRepository.deleteById(id);
    }
}
