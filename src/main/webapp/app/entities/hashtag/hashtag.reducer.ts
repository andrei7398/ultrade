import axios from 'axios';
import { ICrudSearchAction, ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IHashtag, defaultValue } from 'app/shared/model/hashtag.model';

export const ACTION_TYPES = {
  SEARCH_HASHTAGS: 'hashtag/SEARCH_HASHTAGS',
  FETCH_HASHTAG_LIST: 'hashtag/FETCH_HASHTAG_LIST',
  FETCH_HASHTAG: 'hashtag/FETCH_HASHTAG',
  RESET: 'hashtag/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IHashtag>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type HashtagState = Readonly<typeof initialState>;

// Reducer

export default (state: HashtagState = initialState, action): HashtagState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.SEARCH_HASHTAGS):
    case REQUEST(ACTION_TYPES.FETCH_HASHTAG_LIST):
    case REQUEST(ACTION_TYPES.FETCH_HASHTAG):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case FAILURE(ACTION_TYPES.SEARCH_HASHTAGS):
    case FAILURE(ACTION_TYPES.FETCH_HASHTAG_LIST):
    case FAILURE(ACTION_TYPES.FETCH_HASHTAG):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.SEARCH_HASHTAGS):
    case SUCCESS(ACTION_TYPES.FETCH_HASHTAG_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_HASHTAG):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/hashtags';
const apiSearchUrl = 'api/_search/hashtags';

// Actions

export const getSearchEntities: ICrudSearchAction<IHashtag> = (query, page, size, sort) => ({
  type: ACTION_TYPES.SEARCH_HASHTAGS,
  payload: axios.get<IHashtag>(`${apiSearchUrl}?query=${query}`)
});

export const getEntities: ICrudGetAllAction<IHashtag> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_HASHTAG_LIST,
  payload: axios.get<IHashtag>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IHashtag> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_HASHTAG,
    payload: axios.get<IHashtag>(requestUrl)
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
