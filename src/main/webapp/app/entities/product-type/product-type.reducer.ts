import axios from 'axios';
import { ICrudSearchAction, ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProductType, defaultValue } from 'app/shared/model/product-type.model';

export const ACTION_TYPES = {
  SEARCH_PRODUCTTYPES: 'productType/SEARCH_PRODUCTTYPES',
  FETCH_PRODUCTTYPE_LIST: 'productType/FETCH_PRODUCTTYPE_LIST',
  FETCH_PRODUCTTYPE: 'productType/FETCH_PRODUCTTYPE',
  RESET: 'productType/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProductType>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ProductTypeState = Readonly<typeof initialState>;

// Reducer

export default (state: ProductTypeState = initialState, action): ProductTypeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.SEARCH_PRODUCTTYPES):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTTYPE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case FAILURE(ACTION_TYPES.SEARCH_PRODUCTTYPES):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTTYPE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTTYPE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.SEARCH_PRODUCTTYPES):
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTTYPE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTTYPE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/product-types';
const apiSearchUrl = 'api/_search/product-types';

// Actions

export const getSearchEntities: ICrudSearchAction<IProductType> = (query, page, size, sort) => ({
  type: ACTION_TYPES.SEARCH_PRODUCTTYPES,
  payload: axios.get<IProductType>(`${apiSearchUrl}?query=${query}`)
});

export const getEntities: ICrudGetAllAction<IProductType> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PRODUCTTYPE_LIST,
  payload: axios.get<IProductType>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IProductType> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTTYPE,
    payload: axios.get<IProductType>(requestUrl)
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
