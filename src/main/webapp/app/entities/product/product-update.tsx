import React, {useState, useEffect, useCallback} from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getProductTypes } from 'app/entities/product-type/product-type.reducer';
import { getEntities as getProductSizes } from 'app/entities/product-size/product-size.reducer';
import { getEntity, updateEntity, createEntity, reset, getEntities } from './product.reducer';
import Dropzone, {useDropzone} from "react-dropzone";
import { uploadPhotos } from '../photo/photo.reducer';

export interface IProductUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductUpdate = (props: IProductUpdateProps) => {
  const [productTypeId, setProductTypeId] = useState('0');
  const [productSizeId, setProductSizeId] = useState('0');
  const [photos, setPhotos] = useState([]);
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { productEntity, productTypes, productSizes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/product');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getProductTypes();
    props.getProductSizes();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = async (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...productEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity).then(request => {
          props.uploadPhotos(request.value.data.id, photos);
        });
      } else {
        props.updateEntity(entity).then(request => {
          props.uploadPhotos(request.value.data.id, photos);
        });
      }
    }
  };

  function saveFilesInState (files: File[]) {
    setPhotos(files);
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ultradeApp.product.home.createOrEditLabel">
            <Translate contentKey="ultradeApp.product.home.createOrEditLabel">Create or edit a Product</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : productEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="product-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="product-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="genderLabel" for="product-gender">
                  <Translate contentKey="ultradeApp.product.gender">Gender</Translate>
                </Label>
                <AvField id="product-gender" type="text" name="gender" />
              </AvGroup>
              <AvGroup>
                <Label id="numberOfPhotosLabel" for="product-numberOfPhotos">
                  <Translate contentKey="ultradeApp.product.numberOfPhotos">Number Of Photos</Translate>
                </Label>
                <AvField id="product-numberOfPhotos" type="string" className="form-control" name="numberOfPhotos" />
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="product-name">
                  <Translate contentKey="ultradeApp.product.name">Name</Translate>
                </Label>
                <AvField
                  id="product-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
              </AvGroup>
              <Dropzone onDrop={saveFilesInState} accept="application/pdf, image/*" >
                {({ getRootProps, getInputProps, isDragActive }) => (
                  <div {...getRootProps({ borderStyle: 'dotted' })}>
                    <input {...getInputProps()} capture />
                    {isDragActive ?
                      <p>Drop your files here</p> :
                      <p>Drag 'n' drop some files here, or click to select files</p>
                    }
                  </div>
                )}
              </Dropzone>
              <div>
                {productEntity && productEntity.id && (
                  <div>
                    <img src={"api/photos/getPhoto/photo0_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo1_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo2_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo3_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo4_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo5_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo6_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo7_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo8_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                    <img src={"api/photos/getPhoto/photo9_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                  </div>
                )}
              </div>
              <AvGroup>
                <Label for="product-productType">
                  <Translate contentKey="ultradeApp.product.productType">Product Type</Translate>
                </Label>
                <AvInput id="product-productType" type="select" className="form-control" name="productType.id">
                  <option value="" key="0" />
                  {productTypes
                    ? productTypes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="product-productSize">
                  <Translate contentKey="ultradeApp.product.productSize">Product Size</Translate>
                </Label>
                <AvInput id="product-productSize" type="select" className="form-control" name="productSize.id">
                  <option value="" key="0" />
                  {productSizes
                    ? productSizes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.sizeOfProduct}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/product" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  productTypes: storeState.productType.entities,
  productSizes: storeState.productSize.entities,
  productEntity: storeState.product.entity,
  loading: storeState.product.loading,
  updating: storeState.product.updating,
  updateSuccess: storeState.product.updateSuccess
});

const mapDispatchToProps = {
  getProductTypes,
  getProductSizes,
  getEntity,
  updateEntity,
  createEntity,
  reset,
  uploadPhotos,
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductUpdate);
