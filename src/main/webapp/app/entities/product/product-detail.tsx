import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './product.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProductDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductDetail = (props: IProductDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { productEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ultradeApp.product.detail.title">Product</Translate> [<b>{productEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="gender">
              <Translate contentKey="ultradeApp.product.gender">Gender</Translate>
            </span>
          </dt>
          <dd>{productEntity.gender}</dd>
          <dt>
            <span id="numberOfPhotos">
              <Translate contentKey="ultradeApp.product.numberOfPhotos">Number Of Photos</Translate>
            </span>
          </dt>
          <dd>{productEntity.numberOfPhotos}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="ultradeApp.product.name">Name</Translate>
            </span>
          </dt>
          <dd>{productEntity.name}</dd>
          <dt>
            <Translate contentKey="ultradeApp.product.productType">Product Type</Translate>
          </dt>
          <dd>{productEntity.productType ? productEntity.productType.name : ''}</dd>
          <dt>
            <Translate contentKey="ultradeApp.product.productSize">Product Size</Translate>
          </dt>
          <dd>{productEntity.productSize ? productEntity.productSize.sizeOfProduct : ''}</dd>
          {productEntity && productEntity.id && (
            <div>
              <dt>
                <Translate contentKey="ultradeApp.photo.home.title">Photos</Translate>
              </dt>
              <dd>
                <img src={"api/photos/getPhoto/photo0_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo1_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo2_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo3_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo4_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo5_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo6_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo7_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo8_" + productEntity.id + ".jpg"} alt="" width="250px"/>
                <img src={"api/photos/getPhoto/photo9_" + productEntity.id + ".jpg"} alt="" width="250px"/>
              </dd>
            </div>
          )}
        </dl>
        <Button tag={Link} to="/product" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/product/${productEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ product }: IRootState) => ({
  productEntity: product.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
