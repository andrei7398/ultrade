import axios from 'axios';
import { ICrudSearchAction, ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProductSize, defaultValue } from 'app/shared/model/product-size.model';

export const ACTION_TYPES = {
  SEARCH_PRODUCTSIZES: 'productSize/SEARCH_PRODUCTSIZES',
  FETCH_PRODUCTSIZE_LIST: 'productSize/FETCH_PRODUCTSIZE_LIST',
  FETCH_PRODUCTSIZE: 'productSize/FETCH_PRODUCTSIZE',
  RESET: 'productSize/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProductSize>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ProductSizeState = Readonly<typeof initialState>;

// Reducer

export default (state: ProductSizeState = initialState, action): ProductSizeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.SEARCH_PRODUCTSIZES):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTSIZE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTSIZE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case FAILURE(ACTION_TYPES.SEARCH_PRODUCTSIZES):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTSIZE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTSIZE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.SEARCH_PRODUCTSIZES):
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTSIZE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTSIZE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/product-sizes';
const apiSearchUrl = 'api/_search/product-sizes';

// Actions

export const getSearchEntities: ICrudSearchAction<IProductSize> = (query, page, size, sort) => ({
  type: ACTION_TYPES.SEARCH_PRODUCTSIZES,
  payload: axios.get<IProductSize>(`${apiSearchUrl}?query=${query}`)
});

export const getEntities: ICrudGetAllAction<IProductSize> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PRODUCTSIZE_LIST,
  payload: axios.get<IProductSize>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IProductSize> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTSIZE,
    payload: axios.get<IProductSize>(requestUrl)
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
