import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProduct } from 'app/shared/model/product.model';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { IPerson } from 'app/shared/model/person.model';
import { getEntities as getPeople } from 'app/entities/person/person.reducer';
import { getEntity, updateEntity, createEntity, reset } from './advertisement.reducer';
import { IAdvertisement } from 'app/shared/model/advertisement.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IAdvertisementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AdvertisementUpdate = (props: IAdvertisementUpdateProps) => {
  const [productId, setProductId] = useState('0');
  const [sellerId, setSellerId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { advertisementEntity, products, people, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/advertisement');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getProducts();
    props.getPeople();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...advertisementEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ultradeApp.advertisement.home.createOrEditLabel">
            <Translate contentKey="ultradeApp.advertisement.home.createOrEditLabel">Create or edit a Advertisement</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : advertisementEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="advertisement-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="advertisement-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="descriptionLabel" for="advertisement-description">
                  <Translate contentKey="ultradeApp.advertisement.description">Description</Translate>
                </Label>
                <AvField id="advertisement-description" type="text" name="description" />
              </AvGroup>
              <AvGroup check>
                <Label id="reservedLabel">
                  <AvInput id="advertisement-reserved" type="checkbox" className="form-check-input" name="reserved" />
                  <Translate contentKey="ultradeApp.advertisement.reserved">Reserved</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="advertisement-product">
                  <Translate contentKey="ultradeApp.advertisement.product">Product</Translate>
                </Label>
                <AvInput id="advertisement-product" type="select" className="form-control" name="product.id">
                  <option value="" key="0" />
                  {products
                    ? products.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="advertisement-seller">
                  <Translate contentKey="ultradeApp.advertisement.seller">Seller</Translate>
                </Label>
                <AvInput id="advertisement-seller" type="select" className="form-control" name="seller.id">
                  {people
                    ? people.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.firstName + " " + otherEntity.lastName}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/advertisement" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  products: storeState.product.entities,
  people: storeState.person.entities,
  advertisementEntity: storeState.advertisement.entity,
  loading: storeState.advertisement.loading,
  updating: storeState.advertisement.updating,
  updateSuccess: storeState.advertisement.updateSuccess
});

const mapDispatchToProps = {
  getProducts,
  getPeople,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementUpdate);
