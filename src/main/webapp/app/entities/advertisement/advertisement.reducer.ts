import axios from 'axios';
import { ICrudSearchAction, ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IAdvertisement, defaultValue } from 'app/shared/model/advertisement.model';

export const ACTION_TYPES = {
  SEARCH_ADVERTISEMENTS: 'advertisement/SEARCH_ADVERTISEMENTS',
  FETCH_ADVERTISEMENT_LIST: 'advertisement/FETCH_ADVERTISEMENT_LIST',
  FETCH_ADVERTISEMENT: 'advertisement/FETCH_ADVERTISEMENT',
  CREATE_ADVERTISEMENT: 'advertisement/CREATE_ADVERTISEMENT',
  UPDATE_ADVERTISEMENT: 'advertisement/UPDATE_ADVERTISEMENT',
  DELETE_ADVERTISEMENT: 'advertisement/DELETE_ADVERTISEMENT',
  RESET: 'advertisement/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IAdvertisement>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type AdvertisementState = Readonly<typeof initialState>;

// Reducer

export default (state: AdvertisementState = initialState, action): AdvertisementState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.SEARCH_ADVERTISEMENTS):
    case REQUEST(ACTION_TYPES.FETCH_ADVERTISEMENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ADVERTISEMENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ADVERTISEMENT):
    case REQUEST(ACTION_TYPES.UPDATE_ADVERTISEMENT):
    case REQUEST(ACTION_TYPES.DELETE_ADVERTISEMENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.SEARCH_ADVERTISEMENTS):
    case FAILURE(ACTION_TYPES.FETCH_ADVERTISEMENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ADVERTISEMENT):
    case FAILURE(ACTION_TYPES.CREATE_ADVERTISEMENT):
    case FAILURE(ACTION_TYPES.UPDATE_ADVERTISEMENT):
    case FAILURE(ACTION_TYPES.DELETE_ADVERTISEMENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.SEARCH_ADVERTISEMENTS):
    case SUCCESS(ACTION_TYPES.FETCH_ADVERTISEMENT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_ADVERTISEMENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ADVERTISEMENT):
    case SUCCESS(ACTION_TYPES.UPDATE_ADVERTISEMENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ADVERTISEMENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/advertisements';
const apiSearchUrl = 'api/_search/advertisements';

// Actions

export const getSearchEntities: ICrudSearchAction<IAdvertisement> = (query, page, size, sort) => ({
  type: ACTION_TYPES.SEARCH_ADVERTISEMENTS,
  payload: axios.get<IAdvertisement>(`${apiSearchUrl}?query=${query}`)
});

export const getEntities: ICrudGetAllAction<IAdvertisement> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_ADVERTISEMENT_LIST,
  payload: axios.get<IAdvertisement>(apiUrl)
});

export const getEntity: ICrudGetAction<IAdvertisement> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ADVERTISEMENT,
    payload: axios.get<IAdvertisement>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IAdvertisement> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ADVERTISEMENT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IAdvertisement> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ADVERTISEMENT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IAdvertisement> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ADVERTISEMENT,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
