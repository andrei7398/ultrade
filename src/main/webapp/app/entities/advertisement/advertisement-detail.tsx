import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './advertisement.reducer';
import { IAdvertisement } from 'app/shared/model/advertisement.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IAdvertisementDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const AdvertisementDetail = (props: IAdvertisementDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { advertisementEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ultradeApp.advertisement.detail.title">Advertisement</Translate> [<b>{advertisementEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="description">
              <Translate contentKey="ultradeApp.advertisement.description">Description</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.description}</dd>
          <dt>
            <span id="reserved">
              <Translate contentKey="ultradeApp.advertisement.reserved">Reserved</Translate>
            </span>
          </dt>
          <dd>{advertisementEntity.reserved ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="ultradeApp.advertisement.product">Product</Translate>
          </dt>
          <dd>{advertisementEntity.product ? advertisementEntity.product.name : ''}</dd>
          <dt>
            <Translate contentKey="ultradeApp.advertisement.seller">Seller</Translate>
          </dt>
          <dd>{advertisementEntity.seller ? advertisementEntity.seller.firstName + " " + advertisementEntity.seller.lastName : ''}</dd>
        </dl>
        <Button tag={Link} to="/advertisement" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/advertisement/${advertisementEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ advertisement }: IRootState) => ({
  advertisementEntity: advertisement.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementDetail);
