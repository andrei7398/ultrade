import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProductType from './product-type';
import ProductSize from './product-size';
import Hashtag from './hashtag';
import Photo from './photo';
import Product from './product';
import Advertisement from './advertisement';
import Review from './review';
import Message from './message';
import Person from './person';
import Chat from './chat';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}product-type`} component={ProductType} />
      <ErrorBoundaryRoute path={`${match.url}product-size`} component={ProductSize} />
      <ErrorBoundaryRoute path={`${match.url}hashtag`} component={Hashtag} />
      <ErrorBoundaryRoute path={`${match.url}photo`} component={Photo} />
      <ErrorBoundaryRoute path={`${match.url}product`} component={Product} />
      <ErrorBoundaryRoute path={`${match.url}advertisement`} component={Advertisement} />
      <ErrorBoundaryRoute path={`${match.url}review`} component={Review} />
      <ErrorBoundaryRoute path={`${match.url}message`} component={Message} />
      <ErrorBoundaryRoute path={`${match.url}person`} component={Person} />
      <ErrorBoundaryRoute path={`${match.url}chat`} component={Chat} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
