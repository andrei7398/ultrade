import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="user-secret" to="/person">
      <Translate contentKey="global.menu.entities.person" />
    </MenuItem>
    <MenuItem icon="star-half-alt" to="/review">
      <Translate contentKey="global.menu.entities.review" />
    </MenuItem>
    <MenuItem icon="ad" to="/advertisement">
      <Translate contentKey="global.menu.entities.advertisement" />
    </MenuItem>
    <MenuItem icon="box-open" to="/product">
      <Translate contentKey="global.menu.entities.product" />
    </MenuItem>
    <MenuItem icon="tshirt" to="/product-type">
      <Translate contentKey="global.menu.entities.productType" />
    </MenuItem>
    <MenuItem icon="tags" to="/product-size">
      <Translate contentKey="global.menu.entities.productSize" />
    </MenuItem>
    <MenuItem icon="hashtag" to="/hashtag">
      <Translate contentKey="global.menu.entities.hashtag" />
    </MenuItem>
    <MenuItem icon="camera-retro" to="/photo">
      <Translate contentKey="global.menu.entities.photo" />
    </MenuItem>
    <MenuItem icon="comment-dollar" to="/message">
      <Translate contentKey="global.menu.entities.message" />
    </MenuItem>
    <MenuItem icon="comments-dollar" to="/chat">
      <Translate contentKey="global.menu.entities.chat" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
