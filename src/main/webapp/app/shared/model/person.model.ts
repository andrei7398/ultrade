import { IAdvertisement } from 'app/shared/model/advertisement.model';

export interface IPerson {
  id?: number;
  accountType?: string;
  firstName?: string;
  lastName?: string;
  age?: number;
  gender?: string;
  email?: string;
  phoneNumber?: string;
  address?: string;
  advertisements?: IAdvertisement[];
}

export const defaultValue: Readonly<IPerson> = {};
