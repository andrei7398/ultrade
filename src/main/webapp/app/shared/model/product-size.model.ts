export interface IProductSize {
  id?: number;
  name?: string;
  sizeOfProduct?: string;
}

export const defaultValue: Readonly<IProductSize> = {};
