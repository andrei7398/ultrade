export interface IReview {
  id?: number;
  score?: number;
  description?: string;
}

export const defaultValue: Readonly<IReview> = {};
