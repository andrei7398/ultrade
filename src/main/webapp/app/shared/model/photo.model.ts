import { IProduct } from 'app/shared/model/product.model';

export interface IPhoto {
  id?: number;
  extension?: string;
  firstPhoto?: boolean;
  photo?: any;
  product?: IProduct;
}

export const defaultValue: Readonly<IPhoto> = {
  firstPhoto: false
};
