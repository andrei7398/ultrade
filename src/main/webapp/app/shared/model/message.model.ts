import { Moment } from 'moment';

export interface IMessage {
  id?: number;
  time?: Moment;
  message?: string;
}

export const defaultValue: Readonly<IMessage> = {};
