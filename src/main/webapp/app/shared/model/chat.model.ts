export interface IChat {
  id?: number;
}

export const defaultValue: Readonly<IChat> = {};
