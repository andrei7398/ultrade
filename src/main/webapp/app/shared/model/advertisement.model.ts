import { IProduct } from 'app/shared/model/product.model';
import { IPerson } from 'app/shared/model/person.model';

export interface IAdvertisement {
  id?: number;
  description?: string;
  reserved?: boolean;
  product?: IProduct;
  seller?: IPerson;
}

export const defaultValue: Readonly<IAdvertisement> = {
  reserved: false
};
