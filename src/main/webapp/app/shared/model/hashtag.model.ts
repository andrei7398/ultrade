import { IAdvertisement } from 'app/shared/model/advertisement.model';

export interface IHashtag {
  id?: number;
  tag?: string;
  advertisement?: IAdvertisement;
}

export const defaultValue: Readonly<IHashtag> = {};
