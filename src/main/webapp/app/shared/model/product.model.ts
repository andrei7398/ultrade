import { IProductType } from 'app/shared/model/product-type.model';
import { IProductSize } from 'app/shared/model/product-size.model';
import { IPhoto } from 'app/shared/model/photo.model';

export interface IProduct {
  id?: number;
  gender?: string;
  numberOfPhotos?: number;
  name?: string;
  productType?: IProductType;
  productSize?: IProductSize;
  photos?: IPhoto[];
}

export const defaultValue: Readonly<IProduct> = {};
