import './home.scss';

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import {  Row,
          Col,
          Alert,
          Carousel,
          CarouselItem,
          CarouselControl,
          CarouselIndicators,
          CarouselCaption } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';

export type IHomeProp = StateProps;

export const Home = (props: IHomeProp) => {
  const { account } = props;
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const items = [
    {
      src: 'https://hibbettblog.com/wp-content/uploads/2020/01/Air-Jordan-9-Retro-Racer-Blue.jpg',
      altText: 'This',
      caption: 'This'
    },
    {
      src: 'https://tv8.md/wp-content/uploads/2018/05/adidas-1280x720.jpg',
      altText: 'is',
      caption: 'is'
    },
    {
      src: 'https://images.barrons.com/im-101456?width=1280&size=1.77777778',
      altText: 'ULTRADE',
      caption: 'ULTRADE'
    }
  ];

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} />
        {/*<CarouselCaption captionText={item.caption} captionHeader={item.caption} />*/}
        <CarouselCaption captionHeader={item.caption} />
      </CarouselItem>
    );
  });

  return (
    // <Row>
    //   <Col md="12">
    //     <h2 className="text-center">
    //       <Translate contentKey="home.title">Welcome!</Translate>
    //     </h2>
    //     {/* <p className="lead">*/}
    //     {/*  <Translate contentKey="home.subtitle">This is your homepage</Translate>*/}
    //     {/* </p>*/}
    //     {account && account.login ? (
    //       <div className="text-center ml-5 mr-5">
    //         {/*<Alert color="success">*/}
    //         {/*  <Translate contentKey="home.logged.message" interpolate={{ username: account.login }}>*/}
    //         {/*    You are logged in as user {account.login}.*/}
    //         {/*  </Translate>*/}
    //         {/*</Alert>*/}
    //         <Carousel
    //           activeIndex={activeIndex}
    //           next={next}
    //           previous={previous}
    //           className="align-middle"
    //         >
    //           <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
    //           {slides}
    //           <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
    //           <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    //         </Carousel>
    //       </div>
    //     ) : (
    //       <div>
    //         <Alert color="warning">
    //           <Translate contentKey="global.messages.info.authenticated.prefix">If you want to </Translate>
    //           <Link to="/login" className="alert-link">
    //             <Translate contentKey="global.messages.info.authenticated.link"> sign in</Translate>
    //           </Link>
    //           <Translate contentKey="global.messages.info.authenticated.suffix">
    //             , you can try the default accounts:
    //             <br />- Administrator (login=&quot;admin&quot; and password=&quot;admin&quot;)
    //             <br />- User (login=&quot;user&quot; and password=&quot;user&quot;).
    //           </Translate>
    //         </Alert>
    //
    //         <Alert color="warning">
    //           <Translate contentKey="global.messages.info.register.noaccount">You do not have an account yet?</Translate>&nbsp;
    //           <Link to="/account/register" className="alert-link">
    //             <Translate contentKey="global.messages.info.register.link">Register a new account</Translate>
    //           </Link>
    //         </Alert>
    //       </div>
    //     )}
    //   </Col>
    //   {/* <Col md="3" className="pad">*/}
    //   {/*   <span className="hipster rounded" />*/}
    //   {/* </Col>*/}
    // </Row>
    <div>
      {account && account.login ? (
        <div className="text-center ml-5 mr-5">
          {/*<Alert color="success">*/}
          {/*  <Translate contentKey="home.logged.message" interpolate={{ username: account.login }}>*/}
          {/*    You are logged in as user {account.login}.*/}
          {/*  </Translate>*/}
          {/*</Alert>*/}
          {/*<img src="api/photos/getPhoto/first_slide.jpg" alt="myImage"/>*/}
          {/*TODO-aici*/}
          <Carousel
            activeIndex={activeIndex}
            next={next}
            previous={previous}
            className="align-middle"
          >
            <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
            {slides}
            <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
            <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
          </Carousel>
        </div>
      ) : (
        <div>
          <Alert color="warning">
            <Translate contentKey="global.messages.info.authenticated.prefix">If you want to </Translate>
            <Link to="/login" className="alert-link">
              <Translate contentKey="global.messages.info.authenticated.link"> sign in</Translate>
            </Link>
            <Translate contentKey="global.messages.info.authenticated.suffix">
              , you can try the default accounts:
              <br />- Administrator (login=&quot;admin&quot; and password=&quot;admin&quot;)
              <br />- User (login=&quot;user&quot; and password=&quot;user&quot;).
            </Translate>
          </Alert>

          <Alert color="warning">
            <Translate contentKey="global.messages.info.register.noaccount">You do not have an account yet?</Translate>&nbsp;
            <Link to="/account/register" className="alert-link">
              <Translate contentKey="global.messages.info.register.link">Register a new account</Translate>
            </Link>
          </Alert>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
